# Drawing with Python
Herzlich willkommen!
Wir haben für Euch ein paar Aufgaben vorbereitet, damit ihr Python lernt!
Hierfür werden wir [pyCharm](https://www.jetbrains.com/edu-products/download/other-PCE.html) als Entwicklungsumgebung nutzen, darin werden wir in Python programmieren.

## Intro Python Bibliothek Turtle

In diesem Tutorial werden wir die Bibliothek Turtle kennen lernen. Es handelt sich um eine
Python Bibliothek, die uns ermöglicht ein paar coole Zeichnungen zu machen!

Hier ist eine Auflistung der basic Befehle, die sie mitbringt:
### Stift
- `penup()` : Setzt den Stift hoch, wird genutzt um nicht mehr zu zeichnen
- `pendown()` : Setzt den Stift ab, fängt also an zu zeichnen
- `pensize(x)`: Setzt die Stiftgrösse auf x

### Bewegungen
- `forward(x)` : Zeichnet um x Pixel vorwärts  
- `back(x)` : Zeichnet um x Pixel rückwärts
- `left(x)` : Zeichnet um x Pixel links    
- `right(x)` : Zeichnet um x Pixel rechts  

### Farben
- `color('red')` : Zeichnet die Linie mit roter Farbe
- `bgcolor('orange')`: Setzt die Hintergrundfarbe auf orange
- `begin_fill()`: Beginnt das Füllen der Figur
- `end_fill()`: Beendet das Füllen der Figur

### Position
- `goto(x,y)` : Geht zur Koordinate (x,y).

### Figuren
- `circle(x)` : Zeichnet einen Kreis mit Radius x.

Diese Befehle werden wir in den nächsten Tutorials nutzen, um Mandalas zu zeichnen.
Falls du aber nachlesen möchtest, welche weitere Methoden die Bibliothek anbietet,
kannst du sie [hier](https://docs.python.org/3/library/turtle.html) nachschauen.


## Aufgaben
### Einstieg
- [Aufgabe 1: Mandala](./Drawing%20Mandalas/Exercise%201:%20Mandala/Exercise1.md)
- [Aufgabe 2: Rechtecke](./Drawing%20Mandalas/Exercise%202:%20Rectangles/Exercise2.md)
- [Aufgabe 3: Kreis Mandala](./Drawing%20Mandalas/Exercise%203:%20Circle%20Mandala/Exercise3.md)
