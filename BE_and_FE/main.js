

const express = require('express');
const app = express();
app.get('/api/data', (req, res) => {
  res.json({ message: 'Hello from Node.js!' });
});


const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`Server started on port ${PORT}`);
});
