Wer kennt es noch aus der Schule? Die Binomischen Formeln! 
In diesem Tutorial werden wir lernen, wie man eine Formel implementiert, mit mehreren Unbekannten, also Variablen. 
Das werden wir anhand der binomischen Formeln kennen lernen. Hier sind unsere Variablen, die wir den Nutzenden abfragen werden `a und b`.
Hier nochmal ein Auffrischer wie die Formeln aussahen:

![alt text](binForm.png)

1. Als aller Erstes werden wir eine Bibliothek einbinden, die uns später 
bei den Berechnungen helfen wird: 

```import math ```

2. Die erste Formel ist `(a+b)^2`, um die Potenz zu berechnen, werden wir nun die Bibliothek `math` benutzen, die wir am Anfang eingebunden haben.

Die Bibliothek bietet die Methode `math.power(x,y)` an, dabei gibt x die Basis an, und y den Exponenten.
Unsere Basis ist in diesem Fall `a` und unser Exponent `2`. Dies müssen wir für Variable `a` und `b` berechnen.

````python
def erste_formel(a, b):
  powerA = math.pow(a,2)
  powerB = math.pow(b,2)
  middlePart = 2*a*b
````
Den mittleren Teil schreiben wir ebenfalls hin, der eine einfache Multiplikation `2*a*b` ist.
Nun haben wir alle drei Multiplikatanden und müssen sie nur noch aufaddieren und in der Methode zurückgeben.

` return powerA + middlePart + powerB `


Versuche nun das gleiche Vorgehen für die zweite und dritte Formel zu implementieren! Definiere die Methoden mit `zweite_formel(a,b)` und `dritte_formel(a,b)`.


3. So! Nun haben wir unsere drei Formeln und können nung langsam anfangen, diese Methoden aufzurufen. 
Da wir drei Formeln haben und später anhand des Nutzer Inputs die jeweilige Formel ausführen wollen, werden wir die Methode
`fuehre_formel_aus(formelNr, a,b,)` definieren. Wenn die `formelNr == 1` ist, soll die erste Formel ausgeführt werden usw für die restlichen Formeln. 

```python
def fuehre_formel_aus(formelNr,a ,b):
        if formelNr == 1:
            return erste_formel(a,b)
        elif formelNr == 2:
            return zweite_formel(a,b)
        else:
            return dritte_formel(a,b)
```

4. Jetzt haben wir schon unser Grundgerüst, jetzt müssen wir nur noch die Eingaben vom Nutzer einlesen!
Dafür definieren wir eine weitere Methode `run_formula`. Wir fragen den Nutzer welche Formel berechnet werden soll, inkl. Formeln.
Die Eingabe des Nutzers schreiben wir auf unsere Variable `formelNr`.
```python
def run_formula():
    print("Welche Formel soll berechnet werden? ")
    print("1.: (a + b) ^ 2")
    print("2.: (a - b) ^ 2 ")
    print("3.: (a + b) * (a - b)")
    
    formelNr = int(input("Gib die Nummer deiner Wahl ein: "))
```
5. Dann fragen wir nach den Variablen, um unsere Berechnung ausführen zu können 
```python
 a = int(input("Gib die Variable a ein: "))
 b = int(input("Gib die Variable b ein: "))
```
6. Wir rufen dann die Methode `fuehre_formel_aus(formelNr, a, b)`, der wir alle Parameter übergeben.
Wir schreiben das Ergebnis auf die Variable `result`und geben sie aus.


```python
result = switch_formelNr(formelNr,a, b)
    print("Dein Ergebnis ist:" , result)
```

7. Am Ende unseres Codes rufen wir unsere Methode `run_formula()` und starten damit den Code. Führe deinen Code aus (Rechtsklick > Run), dann kannst du unten
in der Konsole mit verschiedenen Eingaben deinen Code testen. 

