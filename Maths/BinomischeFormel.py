import math

def run_formula():
    """
    Expand the binomial formula (x + y)^n
    """
    print("Welche Formel soll berechnet werden? ")
    print("1.: (a + b )^2")
    print("2. (a - b)^2 )")
    print("3. (a + b ) * (a - b)")

    formelNr = int(input("Gib die Nummer deiner Wahl ein: "))
    a = int(input("Gib die Variable a ein: "))
    b = int(input("Gib die Variable b ein: "))
    result = switch_formelNr(formelNr,a, b)
    print("Dein Ergebnis ist:" , result)

def switch_formelNr(formelNr, a, b):
    if formelNr == 1:
        return erste_formel(a,b)
    elif formelNr == 2:
        return zweite_formel(a,b)
    else:
        return dritte_formel(a,b)

def erste_formel( a, b):
    powerA = math.pow(a,2)
    powerB = math.pow(b,2)
    middlePart = 2*a*b
    return powerA + middlePart + powerB

def zweite_formel( a, b):
    powerA = math.pow(a,2)
    powerB = math.pow(b,2)
    middlePart = 2*a*b
    return powerA - middlePart + powerB

def dritte_formel( a, b):
    powerA = math.pow(a,2)
    powerB = math.pow(b,2)
    return powerA - powerB

run_formula()
