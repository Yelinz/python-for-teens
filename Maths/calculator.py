def start_calculation():
   # Frag den User, welche Berechnung er machen will: addieren, subtrahieren, dividieren oder multiplizieren
   print("Wähl eine Berechnung aus:")
   print("1. Addieren")
   print("2. Subtrahieren")
   print("3. Dividieren")
   print("4. Multiplizieren")

   # Lass den User seine Wahl treffen
   wahl = int(input("Gib die Nummer deiner Wahl ein: "))

   # Nimm die notwendigen Eingaben und führe die Berechnung durch
   if wahl == 1:
       print("Du hast Addition gewählt.")
       num1 = float(input("Gib die erste Zahl ein: "))
       num2 = float(input("Gib die zweite Zahl ein: "))
       ergebnis = num1 + num2
       print(f"Das Ergebnis ist {ergebnis}")
   elif wahl == 2:
       print("Du hast Subtraktion gewählt.")
       num1 = float(input("Gib die erste Zahl ein: "))
       num2 = float(input("Gib die zweite Zahl ein: "))
       ergebnis = num1 - num2
       print(f"Das Ergebnis ist {ergebnis}")
   elif wahl == 3:
       print("Du hast Division gewählt.")
       num1 = float(input("Gib die erste Zahl ein: "))
       num2 = float(input("Gib die zweite Zahl ein: "))
       if num2 != 0:
           ergebnis = num1 / num2
           print(f"Das Ergebnis ist {ergebnis}")
       else:
           print("Fehler: Division durch null ist nicht erlaubt.")
   elif wahl == 4:
       print("Du hast Multiplikation gewählt.")
       num1 = float(input("Gib die erste Zahl ein: "))
       num2 = float(input("Gib die zweite Zahl ein: "))
       ergebnis = num1 * num2
       print(f"Das Ergebnis ist {ergebnis}")
   else:
       print("Ungültige Eingabe. Bitte wähl eine Berechnung aus den Optionen 1 bis 4.")

   # Frag den User, ob er eine neue Rechnung starten möchte
   new_calculation = input("Möchtest du eine neue Rechnung starten? (ja/nein): ")
   if new_calculation.lower() == "ja":
       start_calculation()
   else:
       print("Danke für's Benutzen meines Rechners!")

start_calculation()
