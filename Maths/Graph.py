import math

import matplotlib.pyplot as plt

# Data for the graph


# Create the figure and axis
fig, ax = plt.subplots()


# Plot the data



# Set the title and labels
ax.set_title('E function')
ax.set_xlabel('X Axis')
ax.set_ylabel('Y Axis')

x = [1, 2, 3, 4, 5]
y = [2, 4, 6, 8, 10]
# Show the plot
ax.plot(x, y)
#plt.show()


# Exponential
x = []
y = []
for x_val in range(0, 20):
    x.append(x_val)
    y.append(math.pow(math.e, x_val))

ax.plot(x, y)
plt.show()
