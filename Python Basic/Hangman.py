import random

def galgenmaennchen():
    geheimes_wort = 'baumhaus'
    geratene_buchstaben = []
    versuche = 6

    print('Willkommen beim Galgenmännchen!')

    while versuche > 0:
        print('Wort:', "".join(buchstabe if buchstabe in geratene_buchstaben else '_' for buchstabe in geheimes_wort))
        print('Versuche übrig:', versuche)

        eingabe = input('Gib einen Buchstaben ein: ').lower()

        if len(eingabe) == 0 or not eingabe.isalpha():
            print('Bitte mindestens einen Buchstaben eingeben!')
            continue
        elif eingabe in geratene_buchstaben:
            print('Den Buchstaben haben wir schon!')
            continue

        geratene_buchstaben.append(eingabe)

        if eingabe in geheimes_wort:
            print('Der Buchstabe '+ eingabe + ' ist drin!')

        else:
            versuche -= 1
            print('Falscher Buchstabe. Versuche es erneut.')

        if all(buchstabe in geratene_buchstaben for buchstabe in geheimes_wort):
            print('Herzlichen Glückwunsch! Sie haben das Wort '+geheimes_wort+' erraten!')
            break

    print('Keine Versuche mehr übrig! Das Wort war'+ geheimes_wort+ '. Spiel vorbei.')

    nochmal_spielen = input('Möchtest du nochmal spielen? (ja/nein): ')
    if nochmal_spielen.lower() == 'ja':
        galgenmaennchen()
    else:
        print('Vielen Dank fürs Spielen von Galgenmännchen!')

galgenmaennchen()
