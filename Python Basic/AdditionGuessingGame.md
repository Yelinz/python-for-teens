Erstelle eine neue und leere Datei mit Namen `Additionsspiel.py`.
In diesem Spiel werden wir zwei Methoden schreiben, ein Mal `generiere_zahlen` und `starte_spiel`
In `generiere_zahlen` werden wir zwei zufällige Methoden generieren, die die Spielerin addieren soll.
In `starte_spiel` werden wir wiederum `generiere_zahlen` aufrufen

1. Zu allererst werden wir zwei Bibliotheken importieren, eine um zufällige Zahlen zu generieren
und eine um die Zeit zu messen. Das sieht dann so aus:
```python
import random
import time
```

2. Wir eröffnen eine neue Methode `generiere_zahlen` mit 
```python
def generiere_zahlen():
```
3. Wir haben `random` importiert, welches übersetzt zufällig heisst. Mit ihr können wir zwei zufällige Zahlen generieren, aber dafür
müssen wir ihr auch sagen, in welchem Zahlenbereich. Fürs erste soll es eine Zahl zwischen 1-100 nehmen. Später
kannst du sie auch erhöhen, z.B. auf 1000. 
Um eine zufällige zu generiern rufen wir `random.randint(1, 100)` auf.
Nachdem es zwei Nummern generiert hat, soll er sie uns auch zurückgeben, dafür können wir 
einfach `return num1, num2` schreiben. 
```python
def generiere_zahlen():
    num1 = random.randint(1, 100)
    num2 = random.randint(1, 100)
    return num1, num2
```

4. Jetzt geht es weiter mit der nächsten Methode `starte_spiel()`. Wir definieren eine neue Methode
und rufen direkt unsere `generiere_zahlen()` auf.   
Wir wissen, dass sie uns zwei Nummern zurück gibt, daher
definieren wir auch zwei Variablen, in die wir unsere Zahlen speichern. 

```python
def starte_spiel():
    num1, num2 = generiere_zahlen()
    print(f"Was ist {num1} + {num2}?")
```
Mit `print(f"")` steht `f` für `format`, es formattiert den String und füllt die Variablen ein, die wir 
definiert haben, also `num1` und `num2`. Damit er es richtig formattieren kann, müssen wir es in
geschweiften Klammern angeben, also `{num1}`.
5. Als nächstes fragen wir die Spielerin, was die Zahlen addiert ergeben mit `print(f"Was ist {num1} + {num2}?")`
Und addieren die beiden Zahlen für uns, damit wir später die Eingabe der Spielerin mit dem richtigen
Ergebnis vergleichen können. 
```python
  print(f"Was ist {num1} + {num2}?")
  correct_answer = num1 + num2
```
6. Jetzt geht es an den Timer! Mit `time.time()` greifen wir auf den aktuellen Zeitpunkt zu. Da wir die Zeit messen wollen,
wie lange die Spielerin gebraucht hat, müssen wir die Zeit zwei Mal abgreifen.   
Ein Mal nachdem wir unsere Frage gestellt haben 
und ein Mal nachdem die Spielerin ihre Antwort eingegeben hat.   
Die Eingabe greifen wir mit `input()` ab.   
Am Ende subtrahieren wir die Zeiten voneinander und voilà wir haben die Zeit gemessen!
 ```python
  start_time = time.time()
  user_answer = input("Deine Antwort: ")
  end_time = time.time()
  gemessene_zeit = end_time - start_time
```
5. Nun wollen wir die Eingabe der Nutzerin auswerten. Entweder war die Eingabe korrekt oder falsch, dafür nutzen wir ein 
`if-else`.    
Wir konvertieren zuerst die Eingabe mit `int(user_answer)` und vergleichen mit `==` das eingebene Ergebnis mit 
dem richtigen Ergebnis.   
Wenn die beiden Zahlen gleich sind, war die Eingabe richtig, sonst geben wir falsch aus und verraten was
das richtige Ergebnis war. 
 ```python
if int(user_answer) == correct_answer:
    print("Richtig!")
else:
    print(f"Falsch! Die korrekte Antwort ist {correct_answer}")
```
7. Zum Schluss geben wir natürlich auch aus, wie viel Zeit sie gebraucht hat :) 
Dafür haben wir die Zeit ja bereits in `gemessene_zeit` gespeichert, also geben wir das aus. 
 ```python
    print(f"Du hast {gemessene_zeit:.2f} Sekunden gebraucht, um deine Antwort einzugeben.")
```
Dabei gibt es hier eine Besonderheit mit `{gemessene_zeit:.2f}`, das `.2f` gibt an wie viele Nachkommazahlen wir unseren Floats sehen wollen. Du kannst auch Mal probieren es auf
`.5f` zu setzen und du wirst den Unterschied sehen. 

<details>
  <summary>Klicken um Lösung anzuzeigen</summary>

```py
import random
import time

def generiere_zahlen():
    num1 = random.randint(1, 100)
    num2 = random.randint(1, 100)
    return num1, num2


def starte_spiel():
    num1, num2 = generiere_zahlen()
    print(f"Was ist {num1} + {num2}?")
    correct_answer = num1 + num2

    start_time = time.time()
    user_answer = input("Deine Antwort: ")
    end_time = time.time()
    gemessene_zeit = end_time - start_time

    if int(user_answer) == correct_answer:
        print("Richtig!")
    else:
        print(f"Falsch! Die korrekte Antwort ist {correct_answer}")
    print(f"Du hast {gemessene_zeit:.2f} Sekunden gebraucht, um deine Antwort einzugeben.")

starte_spiel()

```
</details>
