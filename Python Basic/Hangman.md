Erstelle eine neue und leere Datei mit Namen `Galgenmaennchen.py`.


1. Wir öffnen eine Funktion mit `def` mit dem Namen galgenmaennchen. Dann definieren wir unser Wort dass der Spielende erraten soll und die Anzahl der Versuche auf 6. Setze dein eigenes Wort! 
In diesem Beispiel ist das geheime Wort Baumhaus. Um die erratenen Buchstaben vom Spielenden zu behalten
legen wir eine Liste `geratene_buchstaben` an, in der wir sie sammeln werden.
```
def galgenmaennchen()
    geheimes_wort = 'baumhaus'
    versuche = 6
    geratene_buchstaben = []
```
2. Wir eröffnen eine `while`-Schleife in die wir so lange laufen, solange versuche > 0 ist, also noch Versuche übrig sind.
Und wir geben ein Informationen zum Wort und wie viele Versuche noch übrig sind.
```
while versuche > 0:
     print('Wort:', "".join(buchstabe if buchstabe in geratene_buchstaben 
            else '_' for buchstabe in geheimes_wort))
     print('Versuche übrig:', versuche)
```
3. Wir nehmen den Input des Spielenden an und mit `lower()` schreiben wir den Buchstaben klein
```
  eingabe = input('Gib einen Buchstaben ein: ').lower()
```
4. Jetzt wollen wir erst Mal überprüfen, ob es eine gültige Eingabe ist! Denn der Spielende kann ja Zahlen oder nichts eingeben (z.B. direkt Enter drücken)
Mit `len()` können wir auf die "length", also die Länge eines Worter prüfen. Mit `continue` überspringen wir den restlichen Code,
weil die Eingabe ja falsch war. Mit `eingabe in geratene_bcuhstaben` prüfen wir ob der eingegebene Buchstabe bereits in unserer
Buchstabenliste ist, falls ja geben wir genau das aus und überspringen wieder den Code.
```
if len(eingabe) == 0 or not eingabe.isalpha():
    print('Bitte mindestens einen Buchstaben eingeben!')
    continue
elif eingabe in geratene_buchstaben:
    print('Den Buchstaben haben wir schon!')
    continue    
```
5. Nach diesen Checks wissen wir, dass es eine gültige Eingabe ist. Wir können jetzt den Buchstaben
in unsere Liste `geratene_buchstaben` hinzufügen.
```
  geratene_buchstaben.append(eingabe)
```
6. Jetzt prüfen wir, ob der eingebene Buchstabe in unserem Wort liegt! Dafür nutzen wir `eingabe in geheimes_wort`
wenn der Buchstabe im geheimen Wort liegt, gibt er `true` zurück, wenn nicht `false`. 
Wenn er drin liegt, informieren wir den Spielenden, wenn nicht sagen wir falscher Buchstabe und ziehen einen Versuch ab.
```
 if eingabe in geheimes_wort:
            print('Der Buchstabe '+ eingabe + ' ist drin!')

        else:
            versuche -= 1
            print('Falscher Buchstabe. Versuche es erneut.')
```
7. Jetzt fehlt nur noch ein Check: Wenn alle Buchstaben korrekt erraten wurden, beglückwunschen wir den Spielenden!
Was müssen wir dafür überprüfen? Ob alle Buchstaben in unserem geheimen Wort in unserer Liste `geratene_buchstaben` liegen.
Mit `for buchstabe in geheimes_wort` iterieren wir mit der Variable `buchstabe` durch unser geheimes Wort und überprüfen dann
mit `buchstabe in geratene_buchstaben`, ob es auch in der Liste drin liegt. Um das alles herum wickeln wir ein `all()`, d.h.
die Bedingung müssen für alle Buchstaben erfüllt sein. Nur dann geben wir den Herzlichen Glückwunsch- Text aus.
```
  if all(buchstabe in geratene_buchstaben for buchstabe in geheimes_wort):
            print('Herzlichen Glückwunsch! 
            Sie haben das Wort '+geheimes_wort+' erraten!')
            break
```
8. Jetzt sind wir mit dem Code in der while-Schleife fertig. Beachte, dass du jetzt eine Einrückung zurück gehst.
Wenn wir aus der while-Schleife kommen, wurden alle Versuche aufgebraucht, das geben wir auch aus.
```  
   print('Keine Versuche mehr übrig! Das Wort war'+ geheimes_wort+ '. 
   Spiel vorbei.')
```
9. Und zum Schluss fragen wir, ob sie nochmal spielen möchten. Falls ja, dann rufen wir wieder unsere
Funktion `hangmang()` auf, die wir zu Beginn definiert haben. 
```
    nochmal_spielen = input('Möchtest du nochmal spielen? (ja/nein): ')
    if nochmal_spielen.lower() == 'ja':
        galgenmaennchen()
    else:
        print('Vielen Dank fürs Spielen von Galgenmännchen!')
```
Und als letztes rufen wir unsere Methode noch auf `galgenmaennchen()`

Pass dabei auf die korrekten Einrückungen auf!


<details>
  <summary>Klicken um Lösung anzuzeigen</summary>

```py
import random

def galgenmaennchen():
    geheimes_wort = 'baumhaus'
    geratene_buchstaben = []
    versuche = 6

    print('Willkommen beim Galgenmännchen!')

    while versuche > 0:
        print('Wort:', "".join(buchstabe if buchstabe in geratene_buchstaben 
                               else '_' for buchstabe in geheimes_wort))
        print('Versuche übrig:', versuche)

        eingabe = input('Gib einen Buchstaben ein: ').lower()

        if len(eingabe) == 0 or not eingabe.isalpha():
            print('Bitte mindestens einen Buchstaben eingeben!')
            continue
        elif eingabe in geratene_buchstaben:
            print('Den Buchstaben haben wir schon!')
            continue

        geratene_buchstaben.append(eingabe)
        
        if eingabe in geheimes_wort:
            print('Der Buchstabe '+ eingabe + ' ist drin!')

        else:
            versuche -= 1
            print('Falscher Buchstabe. Versuche es erneut.')

        if all(buchstabe in geratene_buchstaben for buchstabe in geheimes_wort):
            print('Herzlichen Glückwunsch! Sie haben das Wort '+geheimes_wort+' erraten!')
            break

    print('Keine Versuche mehr übrig! Das Wort war'+ geheimes_wort+ '. Spiel vorbei.')

    nochmal_spielen = input('Möchtest du nochmal spielen? (ja/nein): ')
    if nochmal_spielen.lower() == 'ja':
        galgenmaennchen()
    else:
        print('Vielen Dank fürs Spielen von Galgenmännchen!')

galgenmaennchen()

```
</details>
