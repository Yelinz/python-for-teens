Erstelle eine neue und leere Datei mit Namen `ZahlenRaten.py`.

1. Wir definieren zuerst unsere geheime Zahl und die Anzahl der Versuche. Setze deine eigene Zahl! Hier habe ich sie
einfach Mal auf 40 gesetzt. Die Anzahl der Versuche setzen wir auf 3.
```
geheime_zahl= 40
moegliche_versuche = 3
```
2. Wir eröffnen eine `while`-Schleife, mit der laufen wir immer wieder den gleichen Code durch, solange die Anzahl `moegliche_versuche`
noch grösser als 0 ist, heisst der Spielende hat noch Versuche übrig. Später zählen wir die
Versuche runter, wenn er falsch rät.
```commandline
while moegliche_versuche > 0:
```
3. Wir sagen dem Spielenden wie das Spiel geht, also eine Zahl von 1-100 und die Anzahl der Versuche die er noch hat.
Mit `input()` können wir die eingebene Zahl des Spielenden aufnehmen.
```
 print('Errate die Zahl zwischen 1 und 100 \nDu hast ', str(moegliche_versuche) + ' Versuche')
 eingabe = input('Deine Eingabe:')
```
4. Als nächstes versuchen wir die eingebene Zahl in eine ganze Zahl, zu einem int zu konvertieren.
Falls es keine ganze Zahl ist, fangen wir das mit `except ValueError` ab.
```
 try:
    geratene_zahl = int(eingabe)
 except ValueError:
    print('Ganz vergessen.. nur gerade Zahlen bitte!')
    moegliche_versuche-=1       
```
Gut, jetzt haben wir die Randbedinngung abgefangen und können uns darauf konzentrieren, nachdem
die Zahl erfolgreich konvertiert wurde. Dafür bleiben wir im `try`-Codeblock.
5. Falls der Spielende eine Zahl grösser als 100 eingegeben haben, fangen wir das ab:
```
 if geratene_zahl > 100:
    print('Die Zahl liegt doch unter 100!')
    continue
```
Mit `continue` springen wir aus dem Block, sodass er nicht in die nächsten Checks laufen wird.
6. Jetzt überprüfen wir ob die eingegebene Zahl grösser als unsere Zahl ist, falls das der Fall ist, zählen
wir `moegliche_versuche` eins runter.
```
 if geratene_zahl > geheime_zahl:
    print('Zu hoch! Versuch\'s nochmal!')
    moegliche_versuche-=1
```
7. Das gleiche Spiel wenn die eingegebene Zahl kleiner als unsere Zahl ist:
```
 elif geratene_zahl < geheime_zahl:
    print('Zu niedrig! Versuch\'s nochmal!')
    moegliche_versuche-=1
```
8. Und wenn die eingegebene Zahl weder grösser noch kleiner als unsere Zahl ist, was heisst das dann?
Genau! Sie ist gleich zu unserer Zahl, also wurde sie korrekt erraten!
```  
else:
    print('Richtig geraten!')
```
9. Ganz am Ende, wenn der Spielende aus der all seine Versuche verbraucht hat und wir deshalb aus der `while`-Schleife kommen,
geben wir noch folgenden Text aus:
```
print('Du hattest drei Versuche und hast die Zahl leider nicht erraten :( Die gesuchte Zahl war', geheime_zahl)
```
Pass dabei auf die korrekten Einrückungen auf!


<details>
  <summary>Klicken um Lösung anzuzeigen</summary>

```py
geheime_zahl= 40
moegliche_versuche = 3

while moegliche_versuche > 0:
    # Regel 1 + 2
    print('Errate die Zahl zwischen 1 und 100 \nDu hast ', str(moegliche_versuche) + ' Versuche')
    eingabe = input('Deine Eingabe:')

    try:
        geratene_zahl = int(eingabe)
        # Regel 1
        if geratene_zahl > 100:
            print('Die Zahl liegt doch unter 100!')
            continue
        # Regel 3
        if geratene_zahl > geheime_zahl:
            print('Zu hoch! Versuch\'s nochmal!')
            moegliche_versuche-=1
        # Regel 4
        elif geratene_zahl < geheime_zahl:
             print('Zu niedrig! Versuch\'s nochmal!')
             moegliche_versuche-=1
        # Regel 5
        else:
            print('Richtig geraten!')
    # Regel 7
    except ValueError:
        print('Ganz vergessen.. nur gerade Zahlen bitte!')
        moegliche_versuche-=1
# Regel 6
print('Du hattest drei Versuche und hast die Zahl leider nicht erraten :( Die gesuchte Zahl war', geheime_zahl)



```
</details>
