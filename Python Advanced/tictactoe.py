def print_board(board):
    for row in board:
        print(" | ".join(row))
        print("-" * 5)

def check_winner(board, player):
    for i in range(3):
        if all(board[i][j] == player for j in range(3)) or all(board[j][i] == player for j in range(3)):
            return True
    return all(board[i][i] == player for i in range(3)) or all(board[i][2-i] == player for i in range(3))

def is_board_full(board):
    return all(all(cell != " " for cell in row) for row in board)

def tictactoe():
    board = [[" "]*3 for _ in range(3)]
    players = ["X", "O"]
    current_player = 0

    while True:
        print_board(board)
        row, col = map(int, input("Enter row and column (0-2) separated by space: ").split())

        if board[row][col] == " ":
            board[row][col] = players[current_player]
            if check_winner(board, players[current_player]):
                print_board(board)
                print(f"Player {players[current_player]} wins!")
                break
            elif is_board_full(board):
                print_board(board)
                print("It's a tie!")
                break
            current_player = (current_player + 1) % 2
        else:
            print("That position is already taken. Try again.")

tictactoe()
