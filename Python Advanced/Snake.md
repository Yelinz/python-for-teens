## Kennst du das Retro Spiel Snake? ;) 
## Das werden wir in diesem Tutorial mit `pygame` programmieren!

Erstelle eine Datei mit Name `Snake.py`.
Wir werden in diesem Tutorial das erste Mal Klassen definieren und zwar `Snake` und `Food`.
Die Klasse `Snake` wird die Navigation der Schlange übernehmen und wenn sie Futter gefunden hat, länger werden.
Die Klasse `Food` wird sich immer wieder neu und random im Bild positionieren, wenn sie gefressen wurde. 
Für jede Klasse werden wir jeweils Konstruktoren definieren. 
## Board Setup
Zu Beginn wieder unsere geliebten Bibliotheken importieren und das Spiel initiieren.
```python
import pygame
import random

pygame.init()
```
1. Als nächstes definieren wir einige Konstanten, die für das Spiel benötigt werden: Grösse des Spielfelds, Geschwindigkeit der Schlange und die Farben in denen der Hintergrund, die Schlange und das Futter angezeigt werden sollen.
Die Geschwindigkeit und Feldgrösse kannst du später auch noch beliebig anpassen. 
Dann setzen wir noch ein paar RGB-Farben vom [Color Picker](https://www.w3schools.com/colors/colors_picker.asp), auch hier: du kannst deine eigenen Farben wählen.
Ich habe mich erst Mal für blau, orange und gelb entschieden. 
```python

# Game constants
WIDTH, HEIGHT = 640, 480
BLOCK_SIZE = 20
SPEED = 7

# Colors
BLUE = (0, 153, 255)
ORANGE = (0, 204, 102)
YELLOW = (255, 255, 153)
```

2. Wir setzen die Grösse des Spielfelds mit `display.set_mode()` und geben dem Spiel einen Namen, der dann oben angezeigt werden soll.
```python
screen = pygame.display.set_mode((WIDTH, HEIGHT))
pygame.display.set_caption("Snake Game")
```

## Initialisierung
In den Klassen, werden wir eine Instanziierung vornehmen, in Python wird das mit `__init__()` (zwei Unterstriche vor und nach dem Wort init!) geschrieben.
Die Methode wird auch Konstruktur genannt, heisst jedes Mal wenn man diese Klasse instanziiert, wird die `__init__()`-Methode aufgerufen.  
Im Konstruktor können Anfangsinitialisierung gesetzt werden, z.B. default-Werte setzen, wenn die Klasse initialisiert wird.
Für die Anfangsinitialisierung der Schlange wollen wir z.B. dass sie mit einem Block anfängt und sich zu Beginn nach rechts bewegt. 

Falls du mehr in Tiefe über Konstrukturen lesen willst: https://cito.github.io/byte_of_python/read/class-init.html 


## Klasse Snake
Die Klasse `Snake` wird mehrere Funktionen haben, unter anderem `__init__(self)` , `move(self)`, `eat(self, food)` und `draw(self)`.
Sie wird sich also instanziieren, bewegen, Futter fressen und den Schlangenkörper zeichnen können. 

###  1. `__init__(self):`
Jedes Mal wenn die Klasse Snake instanziiert wird, soll sie in der Mitte des Felds starten und sich nach rechts bewegen. 
Um die Mitte zu ermitteln, halbieren wir einfach jeweils die Höhe und Breite des Spielfelds, heisst die x-Koordinate wird `WIDTH //2` und die 
y-Koordinate `HEIGHT //2` sein.
Wir wollen, dass die Schlange sich zu Beginn in Richtung rechts bewegt, das machen wir mit einem Vektor der aus `(x,y)`besteht. Die Schlange soll sich in x-Richtung bewegen, daher setzen wir  `self.direction = (1, 0)` 

```python
 def __init__(self):
        self.body = [(WIDTH // 2, HEIGHT // 2)]
        self.direction = (1, 0)
```
Zu beachten ist hierbei, dass wir unter `self.body` ein Array halten, welches ein Tupel, also zwei Variablen speichert. Das sind nämlich die x- und y-Koordinate der Schlange.
`self.direction` hält auch ein Tupel, doch fungiert dieser als Vektor, also eine Richtung vorgibt in die sich die Schlange bewegen soll (x,y) = (1,0) = nach rechts, (-1,0 ) = nach links usw.

### 2. `def move(self):`
Zu Beginn des Spiels, ist die Schlange nur ein Block gross und wächst, je mehr Futter sie gefressen hat.
Ihr Kopf ist also zu Beginn an der gleichen Stelle wie ihr Körper, daher können wir die gleichen Koordinaten für den Kopf übernehmen.
Dann müssen wir die neuen Koordinaten des Kopfes berechnen und fügen sie dem `body` hinzu.
```python
def move(self):
    head = self.body[0]
    new_head = (head[0] + self.direction[0] * 
                BLOCK_SIZE, head[1] + self.direction[1] * BLOCK_SIZE)
    self.body.insert(0, new_head)

```

### 3. `def eat(self, food):`
In dieser Methode wollen wir überprüfen, ob die Schlange das Futter gefressen hat. 
Dafür wird das Objekt `food` übergeben, die auch eine Koordinate ist. D.h. wir müssen nur beide Koordinaten überprüfen ob sie dieselbe sind. Ist das der Fall, ist der Schlangenkopf beim Futter angekommen.


Die Methode gibt ein `True` oder `False` zurück, also ob die Schlange beim Futter ist, oder nicht. 
Falls sich dort kein Futter befand, werden wir die Schlange weiterbewegen, heisst dass die letzte Position des Schlangenschwanzes gelöscht wird, mit `self.body.pop()`

````python
def eat(self, food):
        if self.body[0] == food:
            return True
        else:
            self.body.pop()
            return False
````
### 4. `draw(self):`
Wir werden die Schlange als Pixelblock zeichnen, da bietet die Bibliothek `pygame` die Methode `rect`, kurz für rectangle = Rechteck.

Die Methode nimmt drei Parameter an: `rect(surface, color, rect)`: Spielfläche (damit ist das Spielfeld gemeint), Farbe und das Rechteck selbst.
Wir hatten ja den Schlangenkörper in `self.body` als Array initialisiert. Nun kann es sein, dass es mehrere Elemente in dem Array sind, wenn die Schlange
z.B. schon Futter gefressen hat. Daher müssen wir jetzt durch das Array mit einer for-Schleife iterieren.

Dann bauen wir darin unser Rechteckt mit ` (pos[0], pos[1], BLOCK_SIZE, BLOCK_SIZE)`, die ersten zwei Parameter sind die Koordinate, 
dann kommt Breite und Höhe des Rechtecks, welches dieselbe Grösse haben soll, daher übergeben wir `BLOCK_SIZE` zwei Mal.
Jetzt können wir `pygame.draw.rect(screen, ORANGE, rect)` mit den notwendigen Parametern aufrufen. Die Schlange wird orange sein. 

````python
def draw(self):
        for pos in self.body:
            rect = (pos[0], pos[1], BLOCK_SIZE, BLOCK_SIZE)
            pygame.draw.rect(screen, ORANGE, rect)
````
Damit ist unsere Klasse Snake fertig :) 
## Klasse Food 
Auch für Food brauchen wir einen Konstruktur, da wir das Futter an irgendeinen Ort im Spielfeld positionieren wollen, werden wir 
die Bib `random`benutzen. Dabei soll es ja noch innerhalb des Spielfelds landen. 
Wir rufen `random.randint(0, WIDTH - BLOCK_SIZE)` auf und geben den Zahlenbereich vor, der Wert soll zwischen 0 und Breite- Blockgrösse sein, also 640-20 = 620. 
Falls also die random-Zahl 620 ist, passt der Futterblock mit Grösse 20 noch dahin. 
Das Gleiche berechnen wir für die Y-Koordinate.

Da wir sowohl die Schlange als auch das Futter in "Blockgrösse" zeichnen, muss durch die Grösse dividiert und aufgerundet werden, damit es 
mit dem Vergleich der Koordinate klappt. Das machen wir mit `random_x // BLOCK_SIZE * BLOCK_SIZE` für die X-Koordinate und analog für die Y-Koordinate. 


````python
class Food:
     def __init__(self):
            random_x = random.randint(0, WIDTH - BLOCK_SIZE)
            random_y = random.randint(0, HEIGHT - BLOCK_SIZE)
            self.pos = (random_x // BLOCK_SIZE * BLOCK_SIZE , 
                        random_y // BLOCK_SIZE * BLOCK_SIZE)
````
Dann soll sich das Futter natürlich auch selbst zeichnen können. Das Futter wird gelb sein. 
Auch hier werden wir die Methode `pygame.draw.rect()` benutzen, wo wir die Spielfläche, Farbe und die Grösse des Rechtecks übergeben.
```python
 def draw(self):
        pygame.draw.rect(screen, YELLOW, 
                        (self.pos[0], self.pos[1], BLOCK_SIZE, BLOCK_SIZE))
```

## Spiellogik
1. In dieser Methode werden wir die Konstruktoren aufrufen und die Spielsteuerung implementieren. Die Steuerung brauchen wir, um die Schlange zum Futter bewegen zu können.
Wir schreib das in `def start_game():`
````python
def start_game():
    snake = Snake()
    food = Food()

````
Nun lernen wir eine neue Komponente in Python kennen, nämlich die `clock`. 
Mit ihr geben wir dem Spiel den Takt vor, denn ohne sie, würde das Programm alle Codezeilen ausführen und zwar so schnell wie möglich, heisst, man könnte mit dem Spiel 
nicht interagieren. Die `clock` werden wir später aufrufen. 
````python
clock = pygame.time.Clock()
````

2. Da der Nutzer jederzeit die Pfeiltasten UP, DOWN, LEFT oder RIGHT benutzen kann und wir diese abhören wollen, können wir das mit Hilfe
einer `while`-Schleife implementieren. 
Wir definieren `running = True` die wir je nach Bedingung auf `False` setzen können.
````python
    running = True
    while running:
````
3. Die `pygame` Bibliothek bietet die Möglichkeit auf gedrückte Tasten zu überprüfen, dabei handelt es sich um `Events`.
Mit `pygame.event.get()` können wir auf ausgelöste Events prüfen, also Mausklick und Tastaturdrücken.
Mit `pygame.KEYDOWN` können wir auf Tastaturdruck überprüfen.
Nun brauchen wir noch die Tasten, das ist möglich über `pygame.K_UP`, `pygame.K_DOWN`, `pygame.K_LEFT` und `pygame.K_RIGHT`.


### x-y-Achse 
In Python sieht die X-Y-Achse etwas anders aus, sollen heissen, die Y-Achse geht nach "unten" in den positiven Bereich und nach "oben" in den negativen.
Das heisst, wenn wir nach oben bzw unten navigieren wollen, müssen wir später die `direction` für UP auf (0,-1) und DOWN auf (0,1) setzen.

 ![img.png](./axis.png)

4. Je nachdem, welche Taste gedrückt wurde, wollen wir die Bewegung der `snake` ändern. 
Weisst du noch wo wir die Bewegungsrichtung der Schlange speichern? 
Unter `snake.direction` und es ist ein Vektor, hält also zwei Werte, welche die Richtung vorgibt.

Wir implementieren nun die Bewegungsrichtung für `K_UP` und `K_LEFT`
Wenn wir `K_UP` drücken, soll sich die Schlange nach oben bewegen, heisst keine Bewegung auf der X-Achse, dafür aber auf der Y-Achse.
wir setzen `snake.direction = (0, -1)`
Für `K_LEFT` genau andersrum, wir bewegen sie in x-Richtung, aber nicht in y-Richtung, also `(0,-1)`.
````python
    for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_UP: 
                    snake.direction = (0, -1)
                elif event.key == pygame.K_LEFT:
                    snake.direction = (-1, 0)
````
Versuche nun die Bewegungsrichtungen für `pygame.K_DOWN` und `pygame.K_RIGHT` selbst zu implementieren.

5. Nun ist die for-Schleife fertig! Wenn die Richtung gesetzt ist, wollen wir, dass die Schlange sich bewegt.
Das machen wir mit `snake.move()`.
6. Jetzt müssen wir überprüfen, ob die Schlange das Futter gefunden hat, dafür rufen wir die vorher implementierte Methode `eat()` auf.
Wenn die Methode `True` zurück gibt, wurde das Futter gegessen und wir müssen ein neues Futter initialisieren. Falls nicht, 
dann überprüfen wir, ob sich die Schlange an einer der Ränder befindet. Denn dann ist das Spiel auch zu Ende. 

````python
 if snake.eat(food.pos):
    food = Food()      
````    
7. Wenn das nicht der Fall ist, wollen wir überprüfen, ob sich die Schlange denn im "validen" Bereich befindet, also noch im Spielfeld und nicht an einer der Ränder. 
Als Erinnerung, `snake.body`ist ein doppeltes Array, in `snake.body[0]` befindet sich der Kopf und in `snake.body[0][0]` die X-Koordinate des Schlangenkopf und  `snake.body[0][1]` die Y-Koordinate.

Wann ist die Schlange ausserhalb des gültigen Bereichs? 
- wenn die X- oder Y-Koordinate im Minusbereich von 0 sind
- wenn die Y-Koordinate grösser als die Höhe des Spielfelds `HEIGHT` ist
- wenn die X-Koordinate grösser als die Breite des Spielfelds `WIDTH` ist 

Diese Checks sehen dann folgendermaßen aus:
````python
else:
     if (snake.body[0][0] < 0 or snake.body[0][0] >= WIDTH or
                snake.body[0][1] < 0 or snake.body[0][1] >= HEIGHT):
        running = False
````


8. Zum Schluss, bringen wir alles zusammen, indem wir alles zeichnen. 
Wir geben noch den Takt über `clock.tick(SPEED)` mit und mit `pygame.display.flip()` erwarten wir, dass das Spielfeld immer aktualisiert wird. 
```python   
    screen.fill(BLUE)
    snake.draw()
    food.draw()
    pygame.display.flip()
    clock.tick(SPEED)
```
Hier siehst du auch, dass wir den "Takt" vorgeben mit `clock.tick(SPEED)`.

Und in der letzten Zeile starten wir noch das Spiel mit `start_game()`. 
Führe deinen Code aus und spiele Snake! 

### Weitere Erweiterungsmöglichkeiten für das Spiel 
- Versuche die Geschwindigkeit zu erhöhen, je mehr die Schlange gefressen hat. 
- Versuche zwei oder mehr Futter auf der Spielfläche zu spawnen
- Ändere die Farben der Schlange, des Spielfelds und des Futters
- Versuche zwei Schlangen zu spawnen 
- Versuche Hindernisse für die Schlange einzubauen
