import nltk
import random
from nltk.corpus import wordnet
# Make sure to download the WordNet corpus if you haven't already
nltk.download('wordnet')


# Retrieve adjectives from WordNet

adjectives = set()
for syn in wordnet.all_synsets(wordnet.ADJ):
    for lemma in syn.lemmas():
        adjectives.add(lemma.name())


# Convert to a list and print some adjectives

adjectives_list = list(adjectives)

print(random.sample(adjectives_list, 10))
