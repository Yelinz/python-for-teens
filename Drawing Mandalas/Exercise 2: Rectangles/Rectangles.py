import turtle
shelly = turtle.Turtle()

colors = ['red', 'yellow', 'orange', 'blue', 'green', 'purple']
y_position = 30
for n in range(6):
    for m in range(6):
        shelly.color(colors[m])
        shelly.begin_fill()
        for k in range(4):
            shelly.pendown()
            shelly.forward(20)
            shelly.left(90)
            shelly.penup()
        shelly.end_fill()
        shelly.forward(30)
    shelly.goto(0,y_position)
    y_position += 30
turtle.done()
