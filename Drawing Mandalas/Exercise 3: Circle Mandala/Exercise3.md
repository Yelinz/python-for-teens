### Aufgabe 3: Kreis Mandala
Wir wollen ein buntes Mandala mit Kreisen zeichnen. Wir wollen mit einer Reihe anfangen, die 6 Kreise hat, 
in welchem jeder Kreis eine andere Farbe und Grösse hat.  

Diese Reihe an Kreisen soll so aussehen: ![row_circle.gif](../res/row_circle.png)    
Die wollen wir 36 Mal wiederholen, mit einer kleinen Verschiebung, sodass wir ein Mandala erhalten.
Wir werden in dieser Aufgabe mit zwei for-Schleifen arbeiten, die ineinander verschachtelt sind. 
Lasst uns anfangen!

Erstelle eine neue und leere Datei mit Namen `kreis_mandala.py`.
1. Zu Beginn importieren wir die Bibliothek turtle:   
`import turtle`

2. Als nächstes instanziieren wir eine turtle mit Parameternamen `shelly`.
Unsere Schildkröte heisst also shelly.   
`shelly = turtle.Turtle()`

3. Definiere eine Liste mit Farben, aus denen wir später auswählen werden, wie z.B.:   
`colors = ['red', 'yellow', 'orange', 'blue', 'green', 'purple']`
4. Wir definieren eine Variable `kreis_groesse`, sodass wir den Kreisradius später ändern können.
`kreis_groesse = 20`
5. Wir fangen mit der äusseren for-Schleife an, die hat 36 Wiederholungen   
`for m in range(36): `
6. Wir positionieren unseren Stift erst Mal, daher heben wir den Stift an und fahren 200 Einheiten vorwärts: 
```py
shelly.penup()
shelly.forward(200)
```
7. Wir zeichnen  6 Kreise, dafür eröffnen wir eine innere for-Schleife mit 6 Iterationen:   
`for i in range(6):`
8. Nach einer Einrückung setzen wir eine Farbe und füllen den Kreis damit aus 
```py
shelly.color(colors[i])
shelly.begin_fill()
```
9. Jetzt geht es mit dem Zeichnen los! Stift runter und wir zeichnen den Kreis mit `shelly.circle(kreis_groesse)`. 
Danach heben wir den Stift wieder an und bewegen uns ein Stück zurück, damit wir den nächsten Kreis zeichnen können.

```py
shelly.pendown() 
shelly.circle(kreis_groesse)   
shelly.penup()   
shelly.back(20)    
shelly.end_fill()   
```
10. Weil wir ja unterschiedliche Kreisgrössen haben wollen, verkleinern wir von Kreis zu Kreis die Grösse um 2. 
Das machen wir mit dem Befehl `kreis_groesse -= 2`. 
So subtrahiert er den aktuellen Wert immer um 2. Jetzt ist der Code für die innere Schleife fertig.

Nachdem wir eine Reihe mit 6 Kreisen gezeichnet haben, soll er ja von neuem anfangen,
aber dafür müssen wir die Position anpassen und die Kreisgrösse wieder zurücksetzen, da er sonst an falscher Stelle weitermacht. 
Wir sagen ihm er soll um 80 zurückgehen und sich um 20° nach rechts bewegen.
Zusätzlich setzen wir wir `kreis_groesse` zurück auf 20, damit er mir der Ursprungsgrösse wieder anfangen.

```py
kreis_groesse = 20
shelly.back(80)
shelly.right(20)
```

Jetzt sind wir fertig, das können wir nun auch dem Programm sagen mit `turtle.done()`

Und wie sieht es aus? Drücke auf den ![img.png](../res/run.png) Button um dein Mandala zu sehen!
Falls du das Icon nicht siehst, mache ein Rechtsklick auf deine Python-Datei und du kannst 
`Run kreis_mandala` ausführen.

Sieht es bei dir auch so aus? 
![circle_mandala_2.png](../res/circle_mandala.png)

Probiere noch mehr Farben, Abstände und Kreise aus, um ein anderes Mandala zu erhalten! 

Der gesamte Code sollte nun in etwa so aussehen
<details>
  <summary>Klicken um Lösung anzuzeigen</summary>

```py
import turtle
shelly = turtle.Turtle()

colors = ['red', 'yellow', 'orange', 'blue', 'green', 'purple']
kreis_groesse =  20

for m in range(36):
    shelly.penup()
    shelly.forward(200)
    for i in range(6):
        shelly.color(colors[i])
        shelly.begin_fill()
        shelly.pendown()
        shelly.circle(kreis_groesse)
        shelly.penup()
        shelly.back(20)
        shelly.end_fill()
        kreis_groesse -= 2
    kreis_groesse = 20
    shelly.back(80)
    shelly.right(20)
turtle.done()
```
</details>
