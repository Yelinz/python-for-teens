### Aufgabe 1: Mandala
Erstelle eine neue und leere Datei mit Namen `Turtle.py`.

1. Zu Beginn importieren wir die Bibliothek turtle:   
`import turtle`

2. Als nächstes instanziieren wir eine turtle mit Parameternamen `shelly`.
Unsere Schildkröte heisst also shelly.   
`shelly = turtle.Turtle()`

3. Nun setzen wir die Farbe in der shelly malen soll, z.B. blau:  
` shelly.color('blue')`   

4. Nun wollen wir ein Hexagon zeichnen, diese hat 6 Ecken, also schreiben wir eine for-Schleife mit 6 Iterationen.
Um ein Hexagon zu zeichnen bewegen wir uns 100 Schritte vorwärts und dann 60 nach links: 
```py
for i in range(6):
    shelly.color('blue')
    shelly.forward(100)
    shelly.left(60)
```
Achte dabei darauf, dass die Zeilen nach der for-Schleife eine Einrückung hat, nur so versteht
der Compiler, dass der Code darunter zur Schleife gehört und ausgeführt werden soll.

5. Nun sollten wir bereits etwas sehen! Klicke rechts oben auf den grünen ![img.png](../res/run.png) Button. 
Falls du das Icon nicht siehst, mache ein Rechtsklick auf deine Python-Datei und du kannst `Run Turtle` ausführen.
6. Nun solltest du ein blaues
Hexagon wie dieses sehen: 
![img_1.png](../res/one_hexagon.png)

7. Aber eigentlich wollten wir doch ein Mandala zeichnen oder? Genau! Wir zeichnen dieses Hexagon jetzt noch 36 Mal!
Alle mit einem leichten links-shift um 10.
Am Ende fügen wir noch ein `turtle.done()` damit die Zeichnung auch bleibt und das Fenster nicht sofort schliesst.
```py
for i in range(36):
    for i in range(6):
        shelly.color('blue')
        shelly.forward(100)
        shelly.left(60)
    shelly.right(10)
turtle.done()
```    
Drücke nochmals den ![img.png](../res/run.png) Button und schaue shelly zu wie sie dir dein Mandala zeichnet!
Und wie sieht es aus?    
![img_2.png](../res/mandala_hexagon.png)    
Jetzt haben wir ein cooles Mandala!

8. Jetzt pimpen wir es mit mehr Farben! Schreibe dafür eine Liste mit Farben, nach der Zeile wo shelly instantiiert wurde:   
`colors = ['red', 'yellow', 'purple', 'blue', 'green', 'orange']`   
Ersetze die Zeile  `shelly.color('blue')`mit `shelly.color(colors[i])`

Jetzt haben wir ein buntes Mandala!
![mandala.gif](../res/mandala.gif)

Falls du andere Farben benutzen willst, [findest du hier](https://trinket.io/docs/colors) weitere Farben die die turtle Bibliothek unterstützt.

Wenn du mit dieser Aufgabe fertig bist und genügend ausprobiert hast, kannst du hier zur nächsten: [Aufgabe 2: Rechtecke](/Drawing%20Mandalas/Exercise%202:%20Rectangles/Exercise2.md)
